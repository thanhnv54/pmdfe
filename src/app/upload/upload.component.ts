import { UploadService } from './../service/upload/upload.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent implements OnInit {
  @ViewChild('form') form;
  public logs: any = [];
  public id: number;
  constructor(private uploadSV: UploadService, private route: ActivatedRoute, private router: Router) {
  }

  httpOptions = {
    headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
  };

  ngOnInit() {
    console.log('abcd');
    this.getListLogUpload();
  }

  public fileList: FileList;
  fileToUpload: File = null;
  // get file upload
  fileChange(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  reset() {
    this.form.nativeElement.reset();
  }

  postUpload() {
    this.uploadSV.postFile(this.fileToUpload).subscribe(file => {
      console.log(file);
      this.reset();
      this.getListLogUpload();
    }, error => {
      this.getListLogUpload();
      console.log(error);
    })
  }

  subString(name: string) {
    console.log(name.lastIndexOf(':'));
    const lastchar = name.lastIndexOf('\\');
    return name.substr(lastchar+1);
  }

  getParam() {
    this.router.navigate(['./report', { id: this.id }]);
  }

  getListLogUpload() {
    this.uploadSV.getListLog().subscribe(data => {
      this.logs = data;
      console.log(data);
    })
  }
}
