import { ReportComponent } from './report/report.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './upload/upload.component';
import { AuthGuard, Deactivate } from './authentication/auth.guard';

const routes: Routes = [
  { path: '', component:LoginComponent, canActivate: [Deactivate]},
  { path: 'upload', component: UploadComponent, canActivate: [AuthGuard] },
  { path: 'reports/:id', component: ReportComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
 
 }
