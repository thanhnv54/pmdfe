import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  baseURL = 'http://localhost:4300';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
  };

  apiListReport:string = this.baseURL+'/api/file/reports/'
  apiGetFileName:string = this.baseURL+'/api/file/getname/'
  apiDownload:string = this.baseURL+'/api/file/download/'
  
  getListRP(id){
    return this.http.get(this.apiListReport+ id, this.httpOptions)
  }

  getName(id){
    return this.http.get(this.apiGetFileName + id, this.httpOptions)
  }

  download(id){
    const token = sessionStorage.getItem('accessToken');
    const headers = new HttpHeaders().set('authorization', 'Bearer ' + token);
    return this.http.get(this.apiDownload +id, { headers, responseType: 'blob' as 'json' })
  }
}
