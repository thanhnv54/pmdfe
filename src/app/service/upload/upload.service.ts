import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  baseURL='http://localhost:4300';

  constructor(private http: HttpClient) { }

  apiUpload: string = this.baseURL+'/api/file/logs';
  apiUploadFile: string = this.baseURL+'/api/file/uploadFile';

  httpOptions = {
    headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
  };


  // get list
  public getListLog() {
    return this.http.get(this.apiUpload, this.httpOptions);
  }

  //upload
  postFile(fileToUpload: File) {
    const endpoint = this.apiUploadFile;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData, this.httpOptions)
  }
}
