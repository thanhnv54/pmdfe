import { AuthService } from './../service/auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{
  constructor(private auth: AuthService, private myRoute: Router){
   
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.auth.isLoggedIn()){
        console.log('log');
        return true;
      }else{
        console.log('out')
        this.myRoute.navigate(['']);
        return false;
      }
   
  }

}
@Injectable({
  providedIn: 'root'
})

export class Deactivate implements CanActivate {
  constructor(private router: Router){}
    canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(sessionStorage.getItem('accessToken') !== null){
        this.router.navigate(['upload']);
        return false;
      }else{
        console.log('aaaa');
        return true;
      }
  }
}
